plugins {
    java
    application
    id("com.adarshr.test-logger") version "2.0.0"
}

repositories {
    jcenter()
}

dependencies {
    implementation("io.javalin:javalin:3.6.0")
    implementation("org.slf4j:slf4j-api:1.7.29")
    implementation("org.slf4j:slf4j-simple:1.7.29")
    implementation("org.eclipse.jetty:jetty-http")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.1")
    implementation("com.github.spotbugs:spotbugs-annotations:3.1.12")
    implementation("com.google.guava:guava:28.1-jre")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.4.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.4.2")
    testImplementation("com.squareup.okhttp3:okhttp:4.2.2")
    testImplementation("org.mockito:mockito-core:3.1.0")
    testImplementation("net.javacrumbs.json-unit:json-unit:2.11.1")
}

application {
    mainClassName = "io.raindev.transfers.App"
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()
}

tasks {
    wrapper {
        gradleVersion = "6.0.1"
    }
    withType<JavaCompile> {
        sourceCompatibility = "11"
        targetCompatibility = "11"
    }
}
