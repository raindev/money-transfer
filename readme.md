# Money transfer toy web app

## Building the project

`./gradlew build`

## Running tests

`./gradlew test`

## Gradlew wrapper wrapper

To save some typing check out [gdub](http://gdub.rocks).
