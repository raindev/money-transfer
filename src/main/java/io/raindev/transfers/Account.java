package io.raindev.transfers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
class Account {
    /**
     * Balance in cents (to prevent floating point rounding errors).
     */
    private final int balanceEuroCents;

    @JsonCreator public Account(@JsonProperty("balanceEuroCents") int balanceEuroCents) {
        this.balanceEuroCents = balanceEuroCents;
    }

    int balanceEuroCents() {
        return balanceEuroCents;
    }

    @Override public String toString() {
        return "Account{" +
                "balanceEuroCents=" + balanceEuroCents +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return balanceEuroCents == account.balanceEuroCents;
    }

    @Override public int hashCode() {
        return Objects.hash(balanceEuroCents);
    }
}
