package io.raindev.transfers;

import io.javalin.Javalin;
import org.eclipse.jetty.http.HttpStatus;

import java.util.NoSuchElementException;

class App {

    public static void main(String[] args) {
        init(new MoneyService());
    }

    public static Javalin init(MoneyService moneyService) {
        Javalin app = Javalin.create();
        app.put("/accounts/:id", ctx -> moneyService
                .saveAccount(ctx.pathParam("id"), ctx.bodyAsClass(Account.class)));
        app.get("/accounts/:id", ctx -> ctx.json(moneyService.findAccount(ctx.pathParam("id"))
                .orElseThrow()));
        app.post("/transfers", ctx -> {
            if (!moneyService.transfer(ctx.bodyValidator(Transfer.class)
                    .check(App::validateTransfer).get())) {
                ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
            }
        });

        app.exception(NoSuchElementException.class, (exception, ctx) -> ctx.status(HttpStatus.NOT_FOUND_404));
        return app.start(0);
    }

    private static boolean validateTransfer(Transfer transfer) {
        return transfer.fromAccountId() != null
                && transfer.toAccountId() != null
                && transfer.amountEuroCents() != 0;
    }
}
