package io.raindev.transfers;

import com.google.common.util.concurrent.Striped;

import javax.annotation.CheckReturnValue;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

@SuppressWarnings("UnstableApiUsage")
class MoneyService {
    private final Map<String, Account> accounts = new ConcurrentHashMap<>();
    private Striped<Lock> locks = Striped.lock(Runtime.getRuntime().availableProcessors() * 2);

    public Optional<Account> findAccount(String accountId) {
        // Because every modification updates ConcurrentHashMap (and does not modify Account in-place) concurrent writes
        // will be visible without additional synchronization.
        return Optional.ofNullable(accounts.get(accountId));
    }

    private int accountLockIndex(String accountId) {
        return Math.abs(accountId.hashCode() % locks.size());
    }

    public void saveAccount(String accountId, Account account) {
        Lock lock = locks.getAt(accountLockIndex(accountId));
        lock.lock();
        try {
            accounts.put(accountId, account);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Transfers money from one account to another if funds on the sources account are sufficient.
     *
     * @param transfer transfer details
     * @return true if the transfer was successful
     */
    @CheckReturnValue
    public boolean transfer(Transfer transfer) {
        int srcAccountLockIndex = accountLockIndex(transfer.fromAccountId());
        Lock srcAccountLock = locks.getAt(srcAccountLockIndex);
        int dstAccountLockIndex = accountLockIndex(transfer.toAccountId());
        Lock dstAccountLock = locks.getAt(dstAccountLockIndex);
        // keep lock order consistent across threads to prevent deadlocks
        if (srcAccountLockIndex < dstAccountLockIndex) {
            srcAccountLock.lock();
            dstAccountLock.lock();
        } else {
            dstAccountLock.lock();
            srcAccountLock.lock();
        }
        try {
            return transferImpl(transfer);
        } finally {
            // attempt to release both locks
            try {
                srcAccountLock.unlock();
            } finally {
                dstAccountLock.unlock();
            }
        }
    }

    private boolean transferImpl(Transfer transfer) {
        final var sourceAccount = accounts.get(transfer.fromAccountId());
        if (sourceAccount == null) {
            throw new NoSuchElementException(
                    "Invalid transfer: source account " + transfer.fromAccountId() + " does not exist");
        }

        final var destinationAccount = accounts.get(transfer.toAccountId());
        if (destinationAccount == null) {
            throw new NoSuchElementException(
                    "Invalid transfer: destination account " + transfer.fromAccountId() + " does not exist");
        }

        if (transfer.fromAccountId().equals(transfer.toAccountId())) {
            return true;
        }

        if (sourceAccount.balanceEuroCents() < transfer.amountEuroCents()) {
            return false;
        }

        accounts.put(transfer.fromAccountId(),
                new Account(sourceAccount.balanceEuroCents() - transfer.amountEuroCents()));
        accounts.put(transfer.toAccountId(),
                new Account(destinationAccount.balanceEuroCents() + transfer.amountEuroCents()));
        return true;
    }

    /**
     * Calculates sum of balances of all accounts. Will not return a correct value in face of concurrent updates
     * (intended for testing).
     *
     * @return total balance of all accounts
     */
    int totalBalanceEuroCents() {
        return accounts.values().stream().mapToInt(Account::balanceEuroCents).sum();
    }

    /**
     * Number of accounts stored (intended for testing).
     * @return total number of accounts
     */
    int accountCount() {
        return accounts.size();
    }

}
