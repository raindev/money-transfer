package io.raindev.transfers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

class Transfer {
    private final String fromAccountId;
    private final String toAccountId;
    private final int amountEuroCents;

    @JsonCreator
    public Transfer(@JsonProperty("fromAccountId") String fromAccountId,
                    @JsonProperty("toAccountId") String toAccountId,
                    @JsonProperty("amountEuroCents") int amountEuroCents) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amountEuroCents = amountEuroCents;
    }

    String fromAccountId() {
        return fromAccountId;
    }

    String toAccountId() {
        return toAccountId;
    }

    int amountEuroCents() {
        return amountEuroCents;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return amountEuroCents == transfer.amountEuroCents &&
                fromAccountId.equals(transfer.fromAccountId) &&
                toAccountId.equals(transfer.toAccountId);
    }

    @Override public int hashCode() {
        return Objects.hash(fromAccountId, toAccountId, amountEuroCents);
    }

    @Override public String toString() {
        return "Transfer{" +
                "fromAccountId='" + fromAccountId + '\'' +
                ", toAccountId='" + toAccountId + '\'' +
                ", amountEuroCents=" + amountEuroCents +
                '}';
    }
}
