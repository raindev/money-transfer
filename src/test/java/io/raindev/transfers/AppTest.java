package io.raindev.transfers;

import io.javalin.Javalin;
import okhttp3.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.Optional;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.eclipse.jetty.http.HttpStatus.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Contract tests for the web API.
 */
class AppTest {
    private static final MediaType JSON = MediaType.get("application/json");
    private static final OkHttpClient client = new OkHttpClient();
    private static final MoneyService moneyService = mock(MoneyService.class);
    private static Javalin server;
    private static String url;

    @BeforeAll static void initAll() {
        server = App.init(moneyService);
        url = "http://localhost:" + server.port();
    }

    @AfterAll static void tearDownAll() {
        server.stop();
    }

    @AfterEach void tearDown() {
        reset(moneyService);
    }

    @SuppressWarnings("SameParameterValue")
    private Response get(String relativeUrl) throws IOException {
        return client.newCall(new Request.Builder()
                .get().url(url + relativeUrl).build())
                .execute();
    }

    @Test void getAccount() throws IOException {
        when(moneyService.findAccount("account_id")).thenReturn(Optional.of(new Account(199)));

        ResponseBody responseBody = get("/accounts/account_id").body();
        assertNotNull(responseBody);
        assertJsonEquals("{\"balanceEuroCents\": 199}", responseBody.string());
    }

    @Test void getNonExistingAccount() throws IOException {
        when(moneyService.findAccount("account_id"))
                .thenReturn(Optional.empty());

        assertEquals(NOT_FOUND_404, get("/accounts/account_id").code());
    }

    @SuppressWarnings("SameParameterValue")
    private Response put(String relativeUrl, String bodyJson) throws IOException {
        return client.newCall(new Request.Builder()
                .put(RequestBody.create(bodyJson, JSON))
                .url(url + relativeUrl)
                .build()).execute();
    }

    @Test void putAccount() throws IOException {
        assertEquals(
                OK_200,
                put("/accounts/account_id", "{\"balanceEuroCents\": 5}").code());
        verify(moneyService).saveAccount("account_id", new Account(5));
    }

    @Test void putAccountNoBalanceStoresZero() throws IOException {
        assertEquals(
                OK_200,
                put("/accounts/account_id", "{}").code());
        verify(moneyService).saveAccount("account_id", new Account(0));
    }

    @SuppressWarnings("SameParameterValue")
    private Response post(String relativeUrl, String bodyJson) throws IOException {
        return client.newCall(new Request.Builder()
                .post(RequestBody.create(bodyJson, JSON))
                .url(url + relativeUrl)
                .build()).execute();
    }

    @Test void postTransfer() throws IOException {
        when(moneyService.transfer(new Transfer("from_account_id", "to_account_id", 7)))
                .thenReturn(true);
        var transferJson =
                "{\"fromAccountId\": \"from_account_id\", \"toAccountId\": \"to_account_id\", \"amountEuroCents\": 7}";
        assertEquals(OK_200, post("/transfers", transferJson).code());
    }

    @Test void postFailedTransfer() throws IOException {
        when(moneyService.transfer(any())).thenReturn(false);

        var transferJson =
                "{\"fromAccountId\": \"from_account_id\", \"toAccountId\": \"to_account_id\", \"amountEuroCents\": 5}";
        assertEquals(UNPROCESSABLE_ENTITY_422, post("/transfers", transferJson).code());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{\"toAccountId\": \"to_account_id\", \"amountEuroCents\": 7}", // no destination
            "{\"fromAccountId\": \"to_account_id\", \"amountEuroCents\": 7}", // no source
            "{\"fromAccountId\": \"from_account_id\", \"toAccountId\": \"to_account_id\"}" // no amount
            })
    void postInvalidTransfer() throws IOException {
        var transferJson = "{\"toAccountId\": \"to_account_id\", \"amountEuroCents\": 7}";
        assertEquals(BAD_REQUEST_400, post("/transfers", transferJson).code());
    }

}
