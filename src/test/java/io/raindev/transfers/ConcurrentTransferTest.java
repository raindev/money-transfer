package io.raindev.transfers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class ConcurrentTransferTest {
    private static final int INITIAL_ACCOUNTS = 100;
    private final ExecutorService executorService = Executors.newFixedThreadPool(4);
    private final MoneyService moneyService = new MoneyService();
    private final AtomicInteger accountIdCounter = new AtomicInteger();

    @BeforeEach void init() {
        // set up initial account balances before executing concurrent updates
        var maxBalance = 1000_00;
        Random random = new Random();
        for (int i = 0; i < INITIAL_ACCOUNTS; i++) {
            var accountId = String.valueOf(accountIdCounter.getAndIncrement());
            var account = new Account(random.nextInt(maxBalance));
            moneyService.saveAccount(accountId, account);
        }
    }

    @AfterEach void tearDown() {
        executorService.shutdown();
    }

    private String randomInitialAccountId() {
        return String.valueOf(ThreadLocalRandom.current().nextInt(INITIAL_ACCOUNTS));
    }

    /**
     * Performs requested number of concurrent updates (transfers and new accounts creations) via MoneyService. Returns
     * after all the asynchronous operations have completed. Transfers are performed only between accounts initially set
     * up accounts.
     *
     * @param updateNumber number of updates to execute
     * @return total balance of the newly created accounts
     * @throws InterruptedException if thread was interrupted while waiting for task completion
     */
    private int executeUpdates(int updateNumber) throws InterruptedException {
        // parameters of the test run
        var maxInitialBalance = 1000_00;
        var maxTransferAmount = 250;
        var newAccountsPercentage = 1;

        AtomicInteger totalAmount = new AtomicInteger(0);
        AtomicInteger accountsCreated = new AtomicInteger(0);
        AtomicInteger successfulTransfers = new AtomicInteger(0);
        AtomicInteger unsuccessfulTransfers = new AtomicInteger(0);
        AtomicInteger exceptionCount = new AtomicInteger(0);

        // released when all asynchronous updates completed
        CountDownLatch countDownLatch = new CountDownLatch(updateNumber);

        var startTime = System.currentTimeMillis();
        for (int i = 0; i < updateNumber; i++) {
            executorService.submit(() -> {
                try {
                    if (ThreadLocalRandom.current().nextInt(100) < newAccountsPercentage) {
                        // create a new account
                        var initialBalance = ThreadLocalRandom.current().nextInt(maxInitialBalance);
                        moneyService.saveAccount(
                                String.valueOf(accountIdCounter.getAndIncrement()), new Account(initialBalance));
                        totalAmount.addAndGet(initialBalance);
                        accountsCreated.incrementAndGet();
                        // make sure the new account is visible
                    } else {
                        // perform a transfer between two of the initial accounts
                        if (moneyService.transfer(new Transfer(randomInitialAccountId(), randomInitialAccountId(),
                                ThreadLocalRandom.current().nextInt(maxTransferAmount)))) {
                            successfulTransfers.incrementAndGet();
                        } else {
                            unsuccessfulTransfers.incrementAndGet();
                        }
                    }
                } catch (RuntimeException e) {
                    exceptionCount.incrementAndGet();
                    e.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await(10, TimeUnit.SECONDS);
        var elapsedTime = System.currentTimeMillis() - startTime;

        // note that these are not proper benchmark measurements but approximate numbers
        System.out.println("executed "
                + successfulTransfers + " successful "
                + unsuccessfulTransfers + " unsuccessful = "
                + (successfulTransfers.intValue() + unsuccessfulTransfers.intValue())
                + " total transfers and created " + accountsCreated +" accounts in "
                + elapsedTime + "ms");

        // fail the test if any updates failed with an exception
        if (exceptionCount.get() != 0) {
            fail(exceptionCount.get() + " tasks failed");
        }

        return totalAmount.get();
    }

    @Test
    void concurrentUpdates() throws InterruptedException {
        int totalAmount = moneyService.totalBalanceEuroCents();
        totalAmount += executeUpdates(100_000); // warm-up
        totalAmount += executeUpdates(1_000_000);

        assertEquals(accountIdCounter.get(), moneyService.accountCount());
        assertEquals(totalAmount, moneyService.totalBalanceEuroCents());
    }
}
