package io.raindev.transfers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class MoneyServiceTest {

    private MoneyService moneyService;

    @BeforeEach void setUp() {
        moneyService = new MoneyService();
    }

    @Test void saveAndFindAccount() {
        var account = new Account(22);
        moneyService.saveAccount("account_id", account);
        assertEquals(Optional.of(account), moneyService.findAccount("account_id"));
    }

    @Test void findNonExistentAccount() {
        assertEquals(Optional.empty(), moneyService.findAccount("missing_account_id"));
    }

    @Test void transfer() {
        moneyService.saveAccount("src_account_id", new Account(60));
        moneyService.saveAccount("dst_account_id", new Account(10));

        assertTrue(moneyService.transfer(new Transfer(
                "src_account_id", "dst_account_id", 20)));
        assertEquals(Optional.of(new Account(40)), moneyService.findAccount("src_account_id"));
        assertEquals(Optional.of(new Account(30)), moneyService.findAccount("dst_account_id"));
    }

    @Test void transferFromTooLowBalance() {
        moneyService.saveAccount("src_account_id", new Account(5));
        moneyService.saveAccount("dst_account_id", new Account(10));

        assertFalse(moneyService.transfer(new Transfer(
                "src_account_id", "dst_account_id", 20)));
        assertEquals(Optional.of(new Account(5)), moneyService.findAccount("src_account_id"));
        assertEquals(Optional.of(new Account(10)), moneyService.findAccount("dst_account_id"));
    }

    @Test void transferToTheSameAccount() {
        moneyService.saveAccount("account_id", new Account(15));

        assertTrue(moneyService.transfer(new Transfer(
                "account_id", "account_id", 15)));
        assertEquals(Optional.of(new Account(15)), moneyService.findAccount("account_id"));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test void transferIfSourceAccountMissing() {
        moneyService.saveAccount("dst_account_id", new Account(20));
        assertThrows(
                NoSuchElementException.class,
                () -> moneyService.transfer(new Transfer(
                        "src_account_id", "dst_account_id", 10)));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test void transferIfDestinationAccountMissing() {
        moneyService.saveAccount("src_account_id", new Account(20));
        assertThrows(
                NoSuchElementException.class,
                () -> moneyService.transfer(new Transfer(
                        "src_account_id", "dst_account_id", 10)));
    }

    @Test void totalBalanceEuroCents() {
        moneyService.saveAccount("account_id1", new Account(10));
        moneyService.saveAccount("account_id2", new Account(20));
        assertEquals(30, moneyService.totalBalanceEuroCents());
    }

    @Test void accountCount() {
        moneyService.saveAccount("account_id1", new Account(10));
        moneyService.saveAccount("account_id2", new Account(20));
        assertEquals(2, moneyService.accountCount());
    }

}
